import { Weather } from './../interfaces/weather';
import { Observable } from 'rxjs';
import { WeatherService } from './../weather.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'card-fancy-example',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {
  city:string;
  temperature:number;
  image:string;
  lat:number;
  lon:number;
  country;
  weatherData$:Observable<Weather>;
  hasError:Boolean = false;
  errorMessage:string;
  constructor(private route:ActivatedRoute, private WeatherService:WeatherService) { }

  //הרשמה לאובסרססול לכן נשתמש בסבסקרייב
  ngOnInit(): void {
    this.city = this.route.snapshot.params.city;
    this.weatherData$ = this.WeatherService.searchWeatherData(this.city);
    this.weatherData$.subscribe(
      data => {
        this.temperature = data.temperature;
        this.image = data.image;
        this.country = data.country;
        this.lat = data.lat;
        this.lon = data.lon;
      },
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}

import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Book } from './interfaces/book';


@Injectable({
  providedIn: 'root'
})

export class BooksService {

  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the"},
  {title:'War and Peace', author:'Leo Tolstoy', summary:"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."}, 
  {title:'The Magic Mountain', author:'Thomas Mann', summary:"If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text"}];

  public addBooks(){
    setInterval(()=>this.books.push({title:'A new one',author:'New author',summary:'Short summary'}),2000);

  }
  
  /*public getBooks(){
    const booksObservable = new Observable(obs => {
      setInterval(()=>obs.next(this.books),5000)
    });
      return booksObservable
  }*/

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  public getBooks(userId,startAfter){ //הפונקציה בונה רפרנס לבוקס קולקשין 
    this.bookCollection = this.db.collection(`users/${userId}/books`,
    ref => ref.orderBy('title','asc').limit(4).startAfter(startAfter));//לפונקציה קולקשין נכניס את הנתיב של בוקס
    return this.bookCollection.snapshotChanges()
    }

  public getBooks2(userId,startAt){                    
        this.bookCollection = this.db.collection(`users/${userId}/books`, //משתנה שמנתב את הנתיב של הקולקיישן בוקס לתוך הדאטה בייס
        ref => ref.orderBy('title', 'asc').limit(4).startAt(startAt)); //שהקולייקשן יחזור ממויין בסדר עולה לפי טייטל ובהגבלה של מקסימום 4 ספרים, וציון של האלמנט האחרון שאנחנו מושכים כדי שהוא יביא לנו את האלמנט אחד אחריו
        return this.bookCollection.snapshotChanges() //סנאפשוט זאת פונקצייה שמייצרת לי אובסרבל שמאזין לשינויים בקולקיישן של בוקס בדאטה בייס, ומעדכנת ישירות לפי כל שינוי שיש ברשימת הספרים בדאטה בייס //נרשם לאובסרבול של הסנאפשוט, מהקומפוננט
    }

    /*public getBooks(userId){ //הפונקציה בונה רפרנס לבוקס קולקשין 
    this.bookCollection = this.db.collection(`users/${userId}/books`);//לפונקציה קולקשין נכניס את הנתיב של בוקס
    return this.bookCollection.snapshotChanges().pipe(map( //סנפשוטצ'אנגס בונה אובסרבבול שמעדכן אותנו על כל שינוי שיש ברשימת הספרים
      collection => collection.map(
        document =>{
          const data = document.payload.doc.data();
          data.id =document.payload.doc.id; //אנחנו רוצים דם את האיי די של הספר כדי שנוכל לבצע גם פעולות על הספרים
          return data; //מכיל גם איי די המשתנה דאטה מכיל את השדות טיטל ואוטור והם נמצאים בפיילוד
        }
      )
    ))
    
    } */

    deleteBook(Userid:string ,id:string){ //צריך את הספר איי די ואת היוזר אידי כי אין שאילתה שתמצא את הדוקיומנט עם אי די מסויים
      this.db.doc(`users/${Userid}/books/${id}`).delete();
    }

    addBook(userId:string,title:string,author:string){
      const book = {title:title, author:author}; //יצירת משתנה קבוע
      this.userCollection.doc(userId).collection('books').add(book); //הלכנו ליוזר קולקשין והכנסנו לו יוזר אי די ואז ליוזר יש קולקשין שנקרא בוקס ולקולקשין הוספנו ספר
    }

    updatebook(userId:string, id:string, title:string, author:string){
      this.db.doc(`users/${userId}/books/${id}`).update(
        {
          title:title,
          author:author
        }
      )
    }

  
  /*public getBooks(){
    return this.books;
  }
*/
/*
getBooks(userId):Observable<any[]>{

}
*/
  constructor(private db:AngularFirestore) { }//הזרקה לדיבי
}

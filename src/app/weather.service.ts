import { WeatherRaw } from './interfaces/weather-raw';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Weather } from './interfaces/weather';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";   //קריאת ה-איי פי איי מתבצעת דרך האנד פוינט שזה היו- אר-אל
  private KEY = "6f8d5b91bc8a33a8446f4b939ac9373e";
  private IMP = "units=metric";

  constructor(private http:HttpClient) { }// אובייקט של אנגולר שמאפשר לעשות קריאת איי פי איי

  //פונקציה שתבצע את התקשורת עם השרת תקבל עיר ותפלוט את נתוני מזג האוויר באובסרבבול
  //על האובסרבבול נפעיל את הפיפ , שתי פונציות מאפ וארור
  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

//פונקציה שתתריע לנו על שגיאה
  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error')
  }


//transformWeatherData ממירה את הנתונים הגולמיים
  private transformWeatherData(data:WeatherRaw):Weather {
    return{
      name:data.name,
      country:data.sys.country,
      image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
    }
  }
}

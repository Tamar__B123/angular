import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

/**
 * @title Radios with ngModel
 */

@Component({
  selector: 'app-classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {
  bbc: string;
  bbcs: string[] = ['bbc', 'cnn', 'nbc'];

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.bbc = this.route.snapshot.params.bbc;
  }

}
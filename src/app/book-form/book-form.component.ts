import { Book } from './../interfaces/book';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bookform',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Input() title:string; //המשתנים האלה יכנסו דרך האבא
  @Input() author:string;
  @Input() id:string;
  @Input() formType:string;
  @Output() update = new EventEmitter<Book>(); //מה שאנחנו עושים לו אוטפוט זה למעשה איוונט
  @Output() closeEdit = new EventEmitter<null>();

  updateParent(){
    let book:Book = {id:this.id, title:this.title, author:this.author};
    this.update.emit(book); //הפונרציה אימיט תשדר לנו החוצה את לנתוני האב את נתוני הבן ק
    if(this.formType == "Add book"){
      this.title = null;
      this.author = null;
    }
  }

  tellParentToClose(){
    this.closeEdit.emit(); 
  }


  constructor() { }

  ngOnInit(): void {
  }

}

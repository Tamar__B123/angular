import { Postst } from './../interfaces/postst';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  
  posts;
  postsData$:Observable<Postst>;
  hasError:Boolean = false;
  errorMessage:string;


  constructor(private PostsService:PostsService) { }

  ngOnInit(): void {
    this.postsData$ = this.PostsService.searchPostsData();
    this.postsData$.subscribe(
      data => {
        this.posts = data;
      },
      error =>{
        console.log(error.message);
        this.hasError = true;
        this.errorMessage = error.message;
      }
    )
  }

}

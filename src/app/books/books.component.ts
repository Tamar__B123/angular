import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Book } from '../interfaces/book';

@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //books;
  books$;
  books:Book[]; //יצירת מערך של ספרים
  userId:string;
  editstate =[];
  addBookFormOPen = false;
  lastDocumentArrived;
  firstDocumentArrived;

  panelOpenState = false;
  constructor(private booksService:BooksService, public authService:AuthService) { }

  deleteBook(id:string){
    this.booksService.deleteBook(this.userId,id);
  }

  update(book:Book){
    this.booksService.updatebook(this.userId, book.id,book.title,book.author)
  }

  add(book:Book){
    this.booksService.addBook(this.userId,book.title,book.author); 
  }

  nextPage(){
    this.books$ = this.booksService.getBooks(this.userId,this.lastDocumentArrived); 
    this.books$.subscribe(
      docs =>{
        this.lastDocumentArrived = docs[docs.length-1].payload.doc; 
        this.books = [];
        for(let document of docs){
          const book:Book = document.payload.doc.data();
          book.id = document.payload.doc.id; 
          this.books.push(book); 
        }
      }
    )     
  }

  previewsPage(){
    this.books$ = this.booksService.getBooks2(this.userId, this.firstDocumentArrived); //נקרא לפונקציה גטבוקס שכתבנו בסרביס, וניצור את האובסרבל
    this.books$.subscribe( //נרשם לאובסרבול
    docs => { 
      this.firstDocumentArrived = docs[0].payload.doc; //כדי להגיע לאלמנט הראשון שהגיע
      this.books = []; //נאפס את מערך הספרים
      for(let document of docs){ //ונבנה מחדש את מערך הספרים בעזרת לולאה, כך שכל פעם שנקבל דיווח מהסנאפשוט מהסרביס שיש שינוי בקולייקשן בדאטה בייס, נעדכן את המערך של רשימת הספרים , ובעזרת לולאה נכניס את המשתנים לספרים
        const book:Book = document.payload.doc.data(); //כדי שנוציא דאטה מתוך הדוקומנט ושנדע מה הגוף של כל דוקומנט (במקרה של בוקס זה טייטל וסופר)
        book.id = document.payload.doc.id; //האיידי של כל ספר לא נחשב חלק מהדאטה לכן נביא אותו בנפרד
        this.books.push(book); //נוסיף למערך הריק שאיפסנו מקודם, את כל הספרים
      } 
    }
  )
}

  

  ngOnInit(): void {
    //this.booksService.addBooks();
    //this.books$ = this.booksService.getBooks(;
    //this.books$.subscribe(books => this.books = books);
    this.authService.getUser().subscribe(
      user =>{
        this.userId = user.uid;
        console.log(this.userId); 
        this.books$ = this.booksService.getBooks(this.userId,null); //האובסרבבול של בוקס מכיל מידע גולמי
        this.books$.subscribe(
          docs =>{
            this.lastDocumentArrived = docs[docs.length-1].payload.doc,
            this.firstDocumentArrived = docs[0].payload.doc,
            this.books =[];
            for(let document of docs){
              const book:Book = document.payload.doc.data();
              book.id = document.payload.doc.id;
              this.books.push(book);
            }
          }
        )
      }
    )}

}



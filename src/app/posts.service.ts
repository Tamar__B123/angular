import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable, ɵɵresolveBody } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { Postst } from './interfaces/postst';
import { PoststRaw } from './interfaces/postst-raw';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http:HttpClient) { }

  searchPostsData():Observable<Postst>{
    return this.http.get<Postst>(`${this.URL}`).pipe(
      catchError(this.handleError)
    )
    }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
    }

  }
  
  //האתר כאן פתוח לחלוטין ואין לנו את עניין החיפוש ולכן לא נכניס דבר לפונקצייה
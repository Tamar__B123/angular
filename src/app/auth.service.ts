import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { User } from './interfaces/user';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  user:Observable<User | null>;

  private eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ =  this.eventAuthError.asObservable();

  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  }//כשהקונסטר'טור נוצר נתחבר ליוזר באופן הבא

  SignUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password);/*.
      then(res =>{ //הפונקציה מחזירה פרומיס שהוא קונסטרקט שדומה מאוד לאובסרסול רק שבניגוד אליו הפרומיס מאזין אבל יודע אך ורק להתחשב באירוע אחד
      console.log('Succesful login;');//הודעת השגיאה-דיבגינג לקונסול
      this.router.navigate(['/books']);
     }
    )
    .catch(error => { //בשביל הודעות השגיאה
            this.eventAuthError.next(error);
     }
    )*/
  }
  
  login(email:string, password:string){
    return this.afAuth.signInWithEmailAndPassword(email,password);
    /*this.afAuth.
    signInWithEmailAndPassword(email, password)
    .then(res =>{
      console.log(res);
      this.router.navigate(['/books']);
     }
    )*/
  } 

  logout(){
    this.afAuth.signOut();
  }

  getUser():Observable<User | null>{
    return this.user;
  }


  
}
